package com.citi.trading.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 */
public class Pricing {

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	private static final Logger LOGGER = Logger.getLogger(Pricing.class.getName()); 
	//List of all subscribers
	private static final ArrayList<Observer> observers = new ArrayList<Observer>();
	//Holds the observer and the stocks its interested in in a list format
	private static final Map<String, ArrayList<String>> register = new HashMap<String, ArrayList<String>>();
	
	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException
				("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}
		
		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);
			
			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}
	
	/**Add observers */
	public static void subscribe(Observer o, ArrayList<String> a) {
		observers.add(o);
		register.put(o.getClass().getName(), a);
	}
	
	/**remove observers */
	public static void unsubscribe(Observer o) {
		observers.remove(o);
		register.remove(o.getClass().getName());
	}
	
	private static void updateObservers() {
		for(Observer o: observers) {
			//o.update(DATA PARAMS);
		}
	}
	
	/**
	 * New method to work with observer model.
	 */
	public List<PricePoint> getPriceData() {
		LOGGER.info("getPriceData call made");
		try {
			String requestURL = "http://will1.conygre.com:8081/prices/" + symbol + "?periods=" + periods;
			BufferedReader in = new BufferedReader(new InputStreamReader
					(new URL(requestURL).openStream()));
			in.readLine(); // header ... right? No way that could break ...
			List<PricePoint> prices = new ArrayList<PricePoint>();
			String line = "";
			while((line = in.readLine())!=null) {
				PricePoint p = Pricing.parsePricePoint(line);
				p.setStock(symbol);
				prices.add(p);
			}			 
			LOGGER.info("Data received successfully");
			return prices;
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, ex.toString());
			throw new RuntimeException
					("Couldn't retrieve price for " + symbol + ".", ex);
		}
	}
	
	/**
	 * Requests data from the HTTP service and returns it to the caller.
	 */
	@Deprecated
	public List<PricePoint> getPriceData(String symbol, int periods) {
		LOGGER.info("getPriceData call made");
		try {
			String requestURL = "http://will1.conygre.com:8081/prices/" + symbol + "?periods=" + periods;
			BufferedReader in = new BufferedReader(new InputStreamReader
					(new URL(requestURL).openStream()));
			in.readLine(); // header ... right? No way that could break ...
			List<PricePoint> prices = new ArrayList<PricePoint>();
			String line = "";
			while((line = in.readLine())!=null) {
				PricePoint p = Pricing.parsePricePoint(line);
				p.setStock(symbol);
				prices.add(p);
			}			 
			LOGGER.info("Data received successfully");
			return prices;
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, ex.toString());
			throw new RuntimeException
					("Couldn't retrieve price for " + symbol + ".", ex);
		}
	}
	
	/**
	 * Quick test of the component.
	 */
	public static void main(String[] args) {
		Pricing pricing = new Pricing();
		List<PricePoint> MRKPrices = pricing.getPriceData("MRK", 6);
		System.out.println(MRKPrices.toString());
	}
}
