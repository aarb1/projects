import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.Pricing;

public class PricingIntegrationTest {

	@Test
	public void getPriceDataTest() {
		Pricing pricing = new Pricing();
		List<PricePoint> response = pricing.getPriceData("MRK", 3);
		assertEquals(3, response.size());
	}

}
